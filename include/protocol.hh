#ifndef protocol_hh
#define protocol_hh

#include <string>
#include <map>
#include <vector>

using std::vector;
using std::map;
using std::string;

/*
 * Protocol commands, from Reveltronics (C)
 */
struct setup_info {
	uint16_t mem_id;
	uint8_t mem_type;
	uint8_t mem_series;
	uint8_t mem_symbol;
	uint8_t mem_size;
	uint8_t mem_block_size;
	uint8_t mem_block_offs;
	uint8_t mem_page_size;
	uint8_t mem_cmd_byte_mode;
	uint8_t mem_x_mode;
	uint8_t mem_cmd_read;
	uint8_t mem_cmd_write;
	uint8_t mem_cmd_wren;
	uint8_t mem_cmd_ewsr;
	uint8_t mem_cmd_wrsr;
	uint8_t mem_cmd_rdsr;
	uint8_t mem_cmd_ce;
	uint8_t mem_reg_val;
	uint8_t mem_vpp;
	uint8_t mem_bus;
	uint8_t mem_spi_freq_div;
	uint8_t mem_imax;
	uint8_t mem_no_of_dies;
	uint8_t mem_reg2;
};

struct target_info {
	struct setup_info setup;
	uint32_t size;		/* total size in bytes */
	uint32_t bsize;		/* usb read block size */
};

struct packer {
	void pack_data(unsigned char *data, int len, int plen);
	void pack_msg(int mesg, unsigned char *data, int len, int plen);
protected:
	vector<unsigned char> buff;
};

struct target {
	virtual void setup_config(unsigned char *) = 0;
};

struct dev_db {
	void setup_device_list();
protected:
	map <string, const struct target_info *> dlist;
};

struct protocol : public packer, public dev_db {

	int setup_type(const string &);

protected:
	int get_mem_size() { return current->size; }
	int get_blk_size() { return current->bsize; }

	void setup_read_fw_ver();
	void setup_config();
	void setup_read();
	void setup_sync();
	void setup_erase();
	void setup_write();
	void setup_send_buff_start();
	void setup_send_buff_stop();
	void setup_set_hw_vpp();
	void setup_set_hw_bus();

protected:
	const struct target_info *current; 
};

#endif /* protocol_hh */
