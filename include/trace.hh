#ifndef trace_hh
#define trace_hh

#include <string>
#include <iostream>
#include <iomanip>
#include <cstdarg>

using std::cout;
using std::string;

/* vt100 stuff */

static __attribute__((unused)) const char *msg = {"\x1b[1;34m"};
static __attribute__((unused)) const char *imp = {"\x1b[1;31m"};
static __attribute__((unused)) const char *dbg = {"\x1b[1;32m"};

static __attribute__((unused)) const char *err = {"\x1b[1;31m+++err: "};
static __attribute__((unused)) const char *rst = {"\x1b[0m"};

static const char progress[5] = "|/-\\";

namespace vt100 {
static inline void cursor_off()
{
	printf("\33[?25l");
}
static inline void cursor_on()
{
	printf("\33[?25h");
}
};

struct trace {
	trace () {}
	static trace & get () {
		static trace t;
		return t;
	}

	void update_progress(int count)
	{
		write("\33[2K\r%c", progress[count % 4]);
	}
	
	void write(const char* format, ...)
	{
		va_list argp;
		va_start(argp, format);

		cout << msg;
		vprintf(format, argp);
		cout << std::flush << rst;
		va_end(argp);
	}
	
	void print(const char* format, ...)
	{
		va_list argp;
		va_start(argp, format);

		printf(dbg);
		vprintf(format, argp);
		printf(rst);

		va_end(argp);
	}

#ifdef DEBUG
	trace & operator << (const string &s)
	{
		cout << s;

		if (s[s.size() - 1] == '\n')
			cout << rst;

		return *this;
	}
	trace & operator << (int i)
	{
		cout << i;

		return *this;
	}
	trace & operator << (long int i)
	{
		cout << i;

		return *this;
	}
	trace & operator << (double i)
	{
		cout << i;

		return *this;
	}
	trace & operator << (long double i)
	{
		cout << i;

		return *this;
	}
#else
	trace & operator << (const string &) { return *this; }
	trace & operator << (int i) { return *this; }
#endif
};

#define m trace::get() << msg
#define e trace::get() << err
#define inf trace::get() << imp
#define d trace::get() << dbg

#endif /* trace_hh */
