#ifndef utils_hh
#define utils_hh

#include <string>

using std::string;

namespace utils {
	string itoa(int val);
	string itoaz(int val, int zpad);
};

#endif /* utils_hh */