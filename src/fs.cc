/*
 * This file is part of the "rvlprog" software.
 *
 * Copyright (C) 2020, Angelo Dureghello
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the
 * Free Software Foundation
 * 51 Franklin Street, Fifth Floor
 * Boston, MA  02110-1301, USA.
 */

#include "fs.hh"

#include <filesystem>

using namespace std;

namespace fs {
int get_file_size(fstream &f)
{
	int size;

	f.seekg(0, ios_base::end);
	size = f.tellg();
	f.seekg(0, ios_base::beg);

	return size;
}

bool file_exist(const string &name)
{
	return filesystem::exists(name);
}
}
