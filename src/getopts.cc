/*
 * This file is part of the "rvlprog" software.
 *
 * Copyright (C) 2020, Angelo Dureghello
 * Copyright (C) 2020, Reveltronics - provided protocol
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the
 * Free Software Foundation
 * 51 Franklin Street, Fifth Floor
 * Boston, MA  02110-1301, USA.
 */

#include <iostream>
#include <getopt.h>

#include "getopts.hh"
#include "version.hh"

using namespace std;

void getopts::info()
{
	cout << "rvlprog v." << version << "-g" << GIT_VERSION << "\n"
		" (C) 2020, Angelo Dureghello, Trieste, Italy\n";
}

void getopts::usage()
{
	info();
	cout << "Usage: rvlprog OPTION... [FILE]\n"
	     << "Example: ./rvlprog -r -t fl_w25q32 output.bin\n\n"
	     << "Options:\n"
	     << "  -h, --help           this help\n"
	     << "  -t, --type           memory type\n"
	     << "  -r                   read binary content\n"
	     << "  -w                   write binary content and verify\n"
	     << "  -e                   erase chip\n"
	     << "  -v                   verify binary content against FILE\n"
	     << "  -l  --list           display supported device list\n"
	     << "\n";
}

getopts::getopts(int argc, char **argv)
{
	int c;

	if (argc == 1) {
		usage();
		exit(-1);
	}

	for (;;) {
		int option_index = 0;
		static struct option long_options[] = {
			{"help", no_argument, 0, 'h'},
			{"list", no_argument, 0, 'l'},
			{"type", required_argument, 0, 't'},
			{"", no_argument, 0, 'r'},
			{"", no_argument, 0, 'w'},
			{"", no_argument, 0, 'v'},
			{"", no_argument, 0, 'e'},
			{0, 0, 0, 0}
		};

		c = getopt_long(argc, argv, "hlt:rwve",
				long_options, &option_index);

		if (c == -1) {
			if (options.list)
				return;
			if (options.erase && options.type != "")
				return;
			if (optind < argc) {
				while (optind < argc)
					options.nonopts.
						push_back(argv[optind++]);
				break;
			}
			usage();
			exit(1);
		}

		switch (c) {
		case 'h':
			usage();
			exit(-1);
			break;
		case 't':
			options.type = optarg;
			break;
		case 'r':
			options.read = true;
			break;
		case 'w':
			options.write = true;
			break;
		case 'v':
			options.verify = true;
			break;
		case 'l':
			options.list = true;
			break;
		case 'e':
			options.erase = true;
			break;
		default:
			exit(-2);
		}
	}
}
