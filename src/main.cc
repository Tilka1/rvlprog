/*
 * This file is part of the "rvlprog" software.
 *
 * Copyright (C) 2020, Angelo Dureghello
 * Copyright (C) 2020, Reveltronics - provided protocol
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the
 * Free Software Foundation
 * 51 Franklin Street, Fifth Floor
 * Boston, MA  02110-1301, USA.
 */

#include "getopts.hh"
#include "usb_init.hh"
#include "usb.hh"
#include "trace.hh"
#include "version.hh"

int main(int argc, char **argv)
{
	int rval;
	getopts gopts(argc, argv);
	opts options = gopts.get_options();

	if (options.list) {
		usb u;

		u.display_device_list();
		return -1;
	}

	if (!options.erase && options.nonopts.size() != 1) {
		gopts.usage();
		exit(-1);
	}

	if (!options.erase && options.nonopts[0] == "") {
		gopts.usage();
		exit(-1);
	}

	gopts.info();

	usb_init i;

	if (i.detect() != 0)
		exit (-1);

	usb u(i.get_ctx(), i.get_device());

	if (u.setup_type(options.type))
		return -1;

	if (options.erase) {
		if (options.read) {
			/* conflict */
			return -1;
		}
		return u.erase_chip();
	}

	string file_name = options.nonopts[0];

	if (options.read || options.verify)
		rval = u.read_binary(file_name);
	else if (options.write) {
		rval = u.write_binary(file_name);
		if (rval == 0)
			u.verify_binary(file_name);
	}

	return rval;
}
