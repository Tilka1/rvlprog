/*
 * This file is part of the "rvlprog" software.
 *
 * Copyright (C) 2020, Angelo Dureghello
 * Copyright (C) 2020, Reveltronics - provided protocol
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the
 * Free Software Foundation
 * 51 Franklin Street, Fifth Floor
 * Boston, MA  02110-1301, USA.
 */

#include <libusb-1.0/libusb.h>
#include <cassert>
#include <cstdint>
#include <fstream>
#include <algorithm>
#include <cstring>
#include <unistd.h>

#include "usb.hh"
#include "trace.hh"
#include "fs.hh"
#include "utils.hh"

using std::cout;
using std::fstream;
using std::min;

static const int ep_out = 0x01;
static const int ep_out_data = 0x03;
static const int ep_in_ctrl = 0x81;
static const int ep_in_data = 0x82;

static const int max_blk_size = 16384;
static const int max_erase_timeout = 10;

static const char tmp_file_name[] = "/tmp/rvlprog";

usb::usb()
{
	setup_device_list();
}

usb::usb(libusb_context *context, libusb_device *device)
: ctx(context), dev(device)
{
	m << "setting up devices database ...\n";

	setup_device_list();

	if (init()) {
		e << "cannot initialize device, exiting.\n";
		exit(-1);
	}

	if (setup_programmer()) {
		e << "cannot setup programmer, exiting.\n";
		exit(-1);
	}
}

usb::~usb()
{
	if (config)
		libusb_free_config_descriptor(config);
	if (dev_handle)
		libusb_close(dev_handle);
	if (ctx)
		libusb_exit(ctx);
}

void usb::display_device_list()
{
	int i = 0;

	cout << "Supported devices :\n";

	if (dlist.size()) {
		for (auto const& [key, val] : dlist) {
			cout << ++i << ". " << key << "\n";
		}
	}
}

int usb::init(void)
{
	int open_rc = libusb_open(dev, &dev_handle);

	if (open_rc == LIBUSB_ERROR_ACCESS ||
		open_rc == LIBUSB_ERROR_NO_DEVICE) {
		e << "access denied, "
		  << "please check to have proper access rghts.\n";
		exit(-1);
	}

	assert(open_rc == 0);

	int rc = libusb_get_active_config_descriptor(dev, &config);
	assert(rc == 0);

	int filter_interface = -1;

	for (int j = 0; j < config->bNumInterfaces; j++) {
		if (filter_interface >= 0 && j != filter_interface)
			continue;

		if (!attach(j)) {

			int r = libusb_claim_interface(dev_handle, j);
			if(r < 0) {
				e << "cannot claim interface.\n";
				exit(-1);
			}
			return 0;
		}
	}

	return -1;
}

int usb::attach(int interface)
{
	int active = libusb_kernel_driver_active(dev_handle, interface);

	cur_interface = interface;

	if (active != 0) {
		detach();
	}

	m << "device attached to interface: " << interface << "\n";

	return 0;
}

int usb::detach()
{
	return libusb_detach_kernel_driver(dev_handle, cur_interface);
}

void usb::send_end_session()
{
	int len;

	setup_set_hw_vpp();
	libusb_bulk_transfer(dev_handle, ep_out, &buff[0], 32, &len, 0);
	setup_set_hw_bus();
	libusb_bulk_transfer(dev_handle, ep_out, &buff[0], 32, &len, 0);
}

int usb::setup_programmer()
{
	int len;

	setup_read_fw_ver();
	libusb_bulk_transfer(dev_handle, ep_out, &buff[0], 32, &len, 0);
	libusb_bulk_transfer(dev_handle, ep_in_ctrl, inbuff, 32, &len, 0);

	/* TO DO: is it a product code, or std reply ? */
	if (len != 4)
		return -1;

	m << "Revelprog IS connected, fw version " 
		<< utils::itoa(inbuff[1]) << "."
		<< utils::itoaz(inbuff[2], 2) << "\n";

	return 0;
}

int usb::receive_target_content(libusb_device_handle *dev_handle,
					const string &out_file)
{
	int blocks, device_size;
	int len, count = 0;
	int bsize;
	fstream f(out_file.c_str(), fstream::binary | fstream::out);

	device_size = get_mem_size();
	bsize = get_blk_size();

	m << "reading memory content ...\n";

	blocks = device_size / bsize;

	vt100::cursor_off();

	while (blocks--) {
		int chunk = bsize;
		unsigned char *p = inbuff;
		count++;
		while (chunk) {
			libusb_bulk_transfer(
				dev_handle, ep_in_data, p, bsize, &len, 0);

			if (len > 0 && len <= bsize) {
				p += len;
				chunk -= len;
			} else {
				break;
			}
			usleep(100);
		}
		trace::get().update_progress(count);
		f.write((char *)inbuff, bsize);
	}

	vt100::cursor_on();

	libusb_bulk_transfer(dev_handle, ep_in_ctrl, inbuff, 3, &len, 0);
	if (len == 3 &&
		(inbuff[0] == 0x01 && inbuff[1] == 0xb2 && inbuff[2] == 0x00)) {
		trace::get().write("\33[2K\rtransfer completed\n");
		send_end_session();
		return 0;
	}

	e << "transfer not properly completed\n";

	return 1;
}

int usb::read_sequence(const string &out_file)
{
	int len;

	setup_read();
	libusb_bulk_transfer(dev_handle, ep_out, &buff[0], 32, &len, 0);
	libusb_bulk_transfer(dev_handle, ep_in_ctrl, inbuff, 3, &len, 0);

	if (inbuff[0] != 0x01 || inbuff[1] != 0xb1 || inbuff[2] != 0) {
		e << "wrong reply, aborting.\n";
		return -1;
	}

	setup_sync();
	libusb_bulk_transfer(dev_handle, ep_out, &buff[0], 33, &len, 0);

	return receive_target_content(dev_handle, out_file);
}

/*
 * USB endpoints 0x00-0x7F are on the host
 * USB endpoints 0x80-0xFF are on the device
 * USB header
 * struct _URB_HEADER {
 * USHORT      Length;
 * USHORT      Function;
 * USBD_STATUS Status;
 * PVOID       UsbdDeviceHandle;
 * ULONG       UsbdFlags;
 * };
 */
int usb::read_binary(const string &out_file)
{
	int len;

	setup_config();

	libusb_bulk_transfer(dev_handle, ep_out, &buff[0], 33, &len, 0);

	return read_sequence(out_file);
}

int usb::erase_chip()
{
	int len;

	m << "erasing memory ...\n";

	setup_config();
	libusb_bulk_transfer(dev_handle, ep_out, &buff[0], 33, &len, 0);

	/* common to different spi nor, likely a read */
	setup_erase();
	libusb_bulk_transfer(dev_handle, ep_out, &buff[0], 32, &len, 0);

	/* erase progress now, is sends 02 13 */
	int count = 0, to = 0;

	vt100::cursor_off();

	for (;;) {
		if (libusb_bulk_transfer(dev_handle,
			ep_in_ctrl, inbuff, 6, &len, 1000) ==
			LIBUSB_ERROR_TIMEOUT) {
			if (++to == max_erase_timeout) {
				vt100::cursor_on();
				e << "erase timeout, exiting.\n";
				return -1;
			}
			continue;
		}
		if (len == 2 && (inbuff[0] == 0x02 && inbuff[1] == 0x13)) {
			count++;
			trace::get().update_progress(count);
		} else if (len == 6) {
			if (inbuff[0] == 0x02 && inbuff[1] == 0x11 &&
			   *(uint32_t *)&inbuff[2] == 0xffffffff) {
				/* completed signal */
			 	break;
			} else if (inbuff[0] == 0x02 && inbuff[1] == 0x15 &&
			   *(uint32_t *)&inbuff[2] == 0) {
				/* informational message, we have to wait */
			}
		} else {
			vt100::cursor_on();
			e << "error erasing device, exiting.\n";
			return -1;
		}
	}

	vt100::cursor_on();

	trace::get().write("\33[2K\rerase completed\n");

	return 0;
}

int usb::write_binary(const string &out_file)
{
	int len;

	if (erase_chip() != 0)
		return -1;

	setup_write();
	libusb_bulk_transfer(dev_handle, ep_out, &buff[0], 32, &len, 0);

	setup_send_buff_start();
	libusb_bulk_transfer(dev_handle, ep_out, &buff[0], 32, &len, 0);

	/* we have to send 256 bytes blocks */
	fstream f(out_file.c_str(), fstream::binary | fstream::in);
	int size = get_mem_size();

	/* to do: check fsize is consinstent to the type */
	int chunks = size / 256;
	int count = 0, bcount = 0;

	if (!chunks) {
		e << "file seems too small, exiting\n";
		return -1;
	}

	inf << "writing memory ...\n";

	vt100::cursor_off();

	/* Seems also 16384 blocks can be sent, not clear why they
	 * are visible only in some cases.
	 */
	while (chunks) {
		f.read((char *)inbuff, 256);
		libusb_bulk_transfer(dev_handle,
					ep_out_data, inbuff, 256, &len, 0);
		if (len != 256) {
			e << "error writing block\n";
			return -1;
		}
		chunks--;
		count++;
		if (!(count % 64)) {
			bcount++;
			trace::get().update_progress(bcount);
		}
	}
	vt100::cursor_on();
	trace::get().write("\33[2K\r");
	inf << "writing completed !\n";

	/* signal the end */
	setup_send_buff_stop();
	libusb_bulk_transfer(dev_handle, ep_out, &buff[0], 2, &len, 0);

	return 0;
}

int usb::verify_binary(const string &orig_binary)
{
	char buff1[max_blk_size + 1];
	char buff2[max_blk_size + 1];

	if (fs::file_exist(tmp_file_name))
		unlink(tmp_file_name);

	m << "verifying ...\n";

	read_sequence(tmp_file_name);

	fstream f(tmp_file_name, fstream::binary | fstream::in);
	fstream g(orig_binary, fstream::binary | fstream::in);

	int size = min(get_mem_size(), fs::get_file_size(g));

	int bsize = get_blk_size();
	int blocks = size / bsize;
	int count;

	vt100::cursor_off();

	for (count = 0; count < blocks; count++) {
		if (f.eof() || g.eof())
			break;
		f.read(buff1, bsize);
		g.read(buff2, bsize);

		if (memcmp(buff1, buff2, bsize) != 0) {
			unlink(tmp_file_name);
			e << "error comparing data, exiting.\n";
			return -1;
		}
		m << "\x1b[010" << progress[count % 4];
	}

	vt100::cursor_on();

	m << "verify test passed\n";

	unlink(tmp_file_name);

#if 0
	int len;
	unsigned char buff[257];

	/* todo: understand what are following steps
	 * and why they are not working */
	libusb_bulk_transfer(dev_handle, ep_in_ctrl, buff, 3, &len, 0);

	if (buff[0] != 0x01 || buff[1] != 0xb2 || buff[2] != 0x00) {
		e << "error getting last block reply, exiting.\n";
		return -1;
	}

	/* ask 256 block now */
	memset(buff, 0, 32);
	buff[0] = 0x01;
	buff[1] = 0xf5;
	libusb_bulk_transfer(dev_handle, ep_out, buff, 32, &len, 0);

	/* Not clear what to do with this block */
	libusb_bulk_transfer(dev_handle, ep_in_data, buff, 256, &len, 0);

	/* get termination */
	libusb_bulk_transfer(dev_handle, ep_in_ctrl, buff, 6, &len, 0);
	if (buff[0] != 0x02 || buff[1] != 0x21 ||
		*(uint32_t*)&buff[2] != 0xffffffff) {

		e << "!error getting termination, exiting.\n";
		return -1;
	}
#endif

	send_end_session();

	return 0;
}
